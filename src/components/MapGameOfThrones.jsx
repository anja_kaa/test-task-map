import React, {Component} from 'react';
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux';
import { Map, ImageOverlay} from 'react-leaflet';
import {map} from 'lodash';
import HeroGameOfThrones  from './HeroGameOfThrones';



class MapGameOfThrones extends Component {
  constructor(props) {
    super(props);
    this.state = {
      center: {
        lat: 256,
        lng: 512,
      },
      zoom: 1,
      draggable: false,
      dimensions: [1200, 1800],
      markerData: [],
    };
   
  };


  render () {
    const {herosPosition} = this.props;
    const boundOrigin = [0, 0];
    const bounds = [boundOrigin, this.state.dimensions];
    const position = [this.state.center.lat, this.state.center.lng]

    return (     
        <Map
          id="map"
          crs={L.CRS.Simple}
          minZoom={-1}
          bounds={bounds}
          center={position}
          zoom={1}         
          style = {{height: 1024}}
          >
          <ImageOverlay
            url='images/map/map.png'
            bounds={bounds}
            />
            { map(herosPosition, heroPosition =>(
                <HeroGameOfThrones {...heroPosition} key={heroPosition.hero}/>)
            )
            }
        </Map>     
    );
  }
}

function mapStateToProps (state) { 
  const  {herosPosition} = state.message; 
  return ({
    herosPosition
  });
};


export default connect(mapStateToProps,null)(MapGameOfThrones);