import React from 'react';
import { Marker, Popup } from 'react-leaflet';
import leaflet from 'leaflet';

delete L.Icon.Default.prototype._getIconUrl;

const HeroGameOfThrones = ({hero, house,x,y}) => {
 
  leaflet.Icon.Default.mergeOptions({    
    iconUrl: require(`../assets/images/coat-of-arms/${house}.png`),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
  });
    return(
      <Marker
        key={hero}
        position={[x,y]}
        >
        <Popup>
          <span>
            {hero}
          </span>
        </Popup>
      </Marker>
    );
  }

export default HeroGameOfThrones;