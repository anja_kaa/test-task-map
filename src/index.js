import React from 'react';
import ReactDom from 'react-dom';
import {Provider} from 'react-redux';
import {createStore,combineReducers} from 'redux';
import Map from './components/MapGameOfThrones';
import * as  reducers from './reducers/';
import * as  actions from './actions/';

const reducer = combineReducers(reducers);
const store = createStore( reducer);

const sock = new WebSocket('ws://localhost:8080/');

sock.onopen = () => {console.log('open connection')};
sock.onmessage = e =>{   
    return store.dispatch(actions.receive(e.data));
};
sock.onclose = () => {console.log('close connection')};

ReactDom.render(
  <Provider store={store}>
    <Map />
  </Provider>, 
  document.getElementById("app")
  );