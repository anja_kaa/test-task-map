export const RECEIVE_MESSAGE = 'RECEIVE_MESSAGE';

export function receive(message){
  return {
    type: RECEIVE_MESSAGE,
    message
  }
}