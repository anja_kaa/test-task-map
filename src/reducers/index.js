import {RECEIVE_MESSAGE} from '../actions';

const initialState = {
  herosPosition:{}
}

export function message(state=initialState, action) {
  switch(action.type){
    case RECEIVE_MESSAGE:
      const  herosPosition = {...state.herosPosition};
      const value = JSON.parse(action.message)
      const {hero}= value
      if(hero) {
        herosPosition[hero] = value;
      }     
      return {
        herosPosition
      } ;
    default:
      return state;
  }
};

